function [ peak_values, peak_indices ] = find_peaks( value_vector )
%FIND_PEAKS GOes through a vector and identifies points where the behavior
%changes from increasing to decreasing.

% There is a matlab function that does this, however, it is returning
% strange stuff so I wrote my own. Vera 3/26/17.

peak_values = nan(1, length(value_vector));
peak_indices = nan(1, length(value_vector));

% This keeps track of whether we are looking for a change to increasing or
% decreasing values. 1 means we are looking for a change to decreasing, 0
% means we are looking for increasing
looking_for_peak = 1;

% Start on 2 to prevent dimension errors
for i = 2:length(value_vector)
    if looking_for_peak
        if value_vector(i) < value_vector(i - 1)
            % Behavior has changed from increasing to decreasing
            
            % Save the values
            peak_values(i - 1) = value_vector(i - 1);
            peak_indices(i - 1) = (i - 1);
            
            % Tell the program to start looking for valleys
            looking_for_peak = 0;
        end
    else % This is a valley. I'm not collecting these at this point
        if value_vector(i) > value_vector(i - 1)
            % Since we found a valley, now we can start looking for a peak
            looking_for_peak = 1;
        end
    end
end

% Remove nans, don't nobody want those
peak_values(isnan(peak_values)) = [];
peak_indices(isnan(peak_indices)) = [];

end

